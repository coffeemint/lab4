﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Models
{
    public class Base
    {
        private static int idG = 100;
        public string ID { get; set; }

        public Base()
        {
            this.ID = (++idG).ToString();
        }

    }
}
