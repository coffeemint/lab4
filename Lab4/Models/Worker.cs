﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Models
{
    public abstract class Worker : Person
    {
        public string Password { get; set; }
        public bool isThisWorker;
        public Worker()
        {
            isThisWorker = false;
        }
    }

    public class Clerk : Worker
    {
        public Clerk() : base()
        { }

    }

    public class Manager : Worker
    {
        public Manager() : base()
        { }

    }
}
