﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;


namespace Lab4.Models
{
    public class Customer : Person
    {
        public string PhoneNumber { get; set; }
        public string Adress { get; set; }

        public ObservableCollection<Product> Products { get; set; }
        public float Owed { get; set; }

        public Customer()
        {
            Products = new ObservableCollection<Product>();
        }
        public Customer(string name, string phoneNumber, string adress)
        {
            Products = new ObservableCollection<Product>();

            Name = name;
            PhoneNumber = phoneNumber;
            Adress = adress;
        }

        public Customer(string ID, string name, string phoneNumber, string adress) : this(name, phoneNumber, adress)
        {

            this.ID = ID;
        }

        public Customer(string name, string phoneNumber, string street, List<Product> products, string ID, float owed) : this(ID, name, phoneNumber, street)
        {
            Products = new ObservableCollection<Product>(products);
            Owed = owed;


        }

        public override string ToString()
        {
            return Name;
        }

        public void Remove(Product item)
        {
            for (int i = 0; i < this.Products.Count; i++)
            {
                if (this.Products[i].ID == item.ID)
                    this.Products.Remove(Products[i]);
            }
        }
    }

}
