﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Models
{
    public enum ProductType
    {
        Film,
        Game
    }
    public class Product: Base
    {

        public ProductType Type { get; private set; }
        public string Name { get; set; }
        public int DayPrice { get; set; }
        public bool IsInShop { get; set; }
        public Product() : base() { }

        public Product(string Name, int DayPrice, bool IsInShop, string ID, ProductType Type)
        {
            this.Name = Name;
            this.DayPrice = DayPrice;
            this.IsInShop = IsInShop;
            this.ID = ID;
        }
        public Product(string Name, int DayPrice, bool IsInShop, ProductType Type)
        {
            this.Name = Name;
            this.DayPrice = DayPrice;
            this.IsInShop = IsInShop;
            this.Type = Type;

        }

        public Product (Product product)
        {
            this.ID = product.ID;
            this.DayPrice = product.DayPrice;
            this.IsInShop = product.IsInShop;
            this.Name = product.Name;
            this.Type = product.Type;
        }


        public override string ToString()
        {
            return Name + " " + this.ID;
        }
    }
}
