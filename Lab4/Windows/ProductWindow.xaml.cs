﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lab4.Models;

namespace Lab4.Windows
{
    /// <summary>
    /// Логика взаимодействия для ProductWindow.xaml
    /// </summary>
    public partial class ProductWindow : Window
    {
        State state = State.getInstance();
        public ProductWindow()
        {
            InitializeComponent();
            DataContext = state.CurrentProduct;
            Customer currCust = state.SearchCustomerByProduct(state.CurrentProduct);
            if (currCust != null)
                CustomerLabel.Content = currCust;
            else
                CustomerLabel.Content = "nobody rent now";
        }
    }
}
