﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lab4.Models;
using Lab4.Pages;

namespace Lab4.Windows
{
    /// <summary>
    /// Логика взаимодействия для EditCustomerWindow.xaml
    /// </summary>
    public partial class EditCustomerWindow : Window
    {

        State state = State.getInstance();

        public EditCustomerWindow(Customer c)
        {
            InitializeComponent();
            DataContext = c;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (NameBox.Text != null && NameBox.Text != "" && PhoneBox.Text != "" && PhoneBox.Text != null && AdressBox.Text != "" && AdressBox.Text != null)
            {
                ClerkPage page = new ClerkPage();
                this.Close();
            }

            else MessageBox.Show("Incorrect data");

        }

        
    }
}
