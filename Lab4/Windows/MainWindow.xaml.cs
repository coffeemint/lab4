﻿using Lab4.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Lab4.Models;

namespace Lab4.Windows
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        State state = State.getInstance();

        //public static Program MyProgram { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            State state = State.getInstance();
           // CustomerWithProducts.Initial();

            MyFrame.NavigationService.Navigate(new System.Uri("Pages/AuthPage.xaml", UriKind.RelativeOrAbsolute));

            //MyProgram = new Program { };

            //MyProgram.Fill("text.txt");

            this.DataContext = state;

        }

        //private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        //{

        //}

        //private void PasswordBox_PasswordChanged_1(object sender, RoutedEventArgs e)
        //{

        //}

        //private void Button_Click(object sender, RoutedEventArgs e)
        //{
        //    passBox1.Visibility = Visibility.Visible;
        //    clerkButton.Visibility = Visibility.Collapsed;
        //    managerButton.Visibility = Visibility.Collapsed;
        //    InputButton.Visibility = Visibility.Visible;
        //}

        //private void InputButton_Click(object sender, RoutedEventArgs e)
        //{
        //    if (passBox1.Password != MyProgram.Clerk.Password)
        //    { MessageBox.Show("Wrong pass!"); }
        //    else
        //    { };
        //}

        private void Frame_Navigated(object sender, NavigationEventArgs e)
        {

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ModelParser<ObservableCollection<Customer>>.SaveJson("customer.json", state.Customers);
            ModelParser<Clerk>.SaveJson("clerk.json", state.Clerk);
            ModelParser<Manager>.SaveJson("manager.json", state.Manager);
            ModelParser<ObservableCollection<Product>>.SaveJson("Products.json", state.Products);
            ModelParser<ObservableCollection<Product>>.SaveJson("Products.json", state.Products);




        }
    }
}
