﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lab4.Pages;
using Lab4.Models;

namespace Lab4.Windows
{
    /// <summary>
    /// Логика взаимодействия для EditProductWindow.xaml
    /// </summary>
    public partial class EditProductWindow : Window
    {
        State state = State.getInstance();
        Product product;

        public EditProductWindow()
        {
            product = new Product(state.CurrentProduct);
            InitializeComponent();
            DataContext = product;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (NameBox.Text.Length > 2 && PriceBox.Text != null && PriceBox.Text != "" && Int32.TryParse(PriceBox.Text, out int i)) 
            {
                state.CurrentProduct.Name = product.Name;
                state.CurrentProduct.DayPrice = product.DayPrice;
                ClerkPage page = new ClerkPage();
                this.Close();
            }

            else MessageBox.Show("Incorrect data");

        }

    }
}
