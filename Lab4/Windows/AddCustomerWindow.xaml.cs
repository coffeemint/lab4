﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lab4.Models;

namespace Lab4.Windows
{
    /// <summary>
    /// Логика взаимодействия для TaskWindow.xaml
    /// </summary>
    public partial class AddCustomerWindow : Window
    {
        State state = State.getInstance();

        public AddCustomerWindow()
        {
            InitializeComponent();
            DataContext = state.Customers;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (NameBox.Text != "" && PhoneBox.Text != "" && StreetBox.Text != "")
            {
                IncorrectBox.Visibility = Visibility.Collapsed;
                Customer Customer = new Customer(NameBox.Text, PhoneBox.Text, StreetBox.Text);
                state.AddCustomer(Customer);
                this.Close();
            }
            else
            {
                IncorrectBox.Visibility = Visibility.Visible;
            }

        }


    }
}
