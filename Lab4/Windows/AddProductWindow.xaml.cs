﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Lab4.Models;

namespace Lab4.Windows
{
    /// <summary>
    /// Логика взаимодействия для AddProductWindow.xaml
    /// </summary>
    public partial class AddProductWindow : Window
    {
        State state = State.getInstance();
        ProductType type;
        public AddProductWindow()
        {

            InitializeComponent();
            TypeBox.ItemsSource = Enum.GetValues(typeof(ProductType)).Cast<ProductType>();
            TypeBox.SelectedIndex = 0;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            int i = 0;
            if (NameBox.Text.Length > 2 && PriceBox.Text != null && PriceBox.Text != "" && Int32.TryParse(PriceBox.Text, out i) && i>0)
            {

                if (TypeBox.SelectedIndex == 0)
                    type = ProductType.Film;
                else
                    type = ProductType.Game;
                state.Products.Add(new Product(NameBox.Text, Int32.Parse(PriceBox.Text), true, type));
                this.Close();
            }

            else MessageBox.Show("Incorrect data");

        }
    }
}
