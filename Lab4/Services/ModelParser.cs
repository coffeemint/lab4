﻿using Lab4.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Lab4.Services
{
    public static class ModelParser<T>
    {
        public static ObservableCollection<T> LoadJsonItems(string fileName)
        {
            using (StreamReader r = new StreamReader(fileName))
            {
                string json = r.ReadToEnd();
                ObservableCollection<T> items = JsonConvert.DeserializeObject<ObservableCollection<T>>(json);
                return items;
            }
        }

        public static ObservableCollection<T> LoadJsonItems(byte[] bArray)
        {
            using (var stream = new MemoryStream(bArray))
            using (var r = new StreamReader(stream, Encoding.UTF8))
            {
                string json = r.ReadToEnd();
                ObservableCollection<T> items = JsonConvert.DeserializeObject<ObservableCollection<T>>(json);
                return items;
            }
        }

        public static T LoadJsonItem(byte[] bArray)
        {
            using (var stream = new MemoryStream(bArray))
            using (var r = new StreamReader(stream, Encoding.UTF8))
            {
                string json = r.ReadToEnd();
                return JsonConvert.DeserializeObject<T>(json);
            }
        }

        public static T LoadJsonItem(string fileName)
        {
            using (StreamReader r = new StreamReader(fileName))
            {
                string json = r.ReadToEnd();
                T item = JsonConvert.DeserializeObject<T>(json);
                return item;
            }
        }

        public static void SaveJson(string name, T value)
        {
            using (StreamWriter file = new StreamWriter(name, false))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, value);
            }
        }
    }
}
