﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lab4.Models;
using Lab4.Windows;

namespace Lab4.Pages
{
    /// <summary>
    /// Логика взаимодействия для ClerkPage.xaml
    /// </summary>
    public partial class ClerkPage : Page
    {

        State state = State.getInstance();

        public ClerkPage()
        {
            InitializeComponent();
            DataContext = state;
            if (state.Clerk.isThisWorker == true)
            {
                DeleteMenuItem.Visibility = Visibility.Collapsed;
                AddMenuItem.Visibility = Visibility.Collapsed;
                DeleteCustomer.Visibility = Visibility.Collapsed;
            }
            else
            {
                DeleteMenuItem.Visibility = Visibility.Visible;
                AddMenuItem.Visibility = Visibility.Visible;
                DeleteCustomer.Visibility = Visibility.Visible;
            }
        }

        private void ProductList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }


        private void AddCustomer_Click(object sender, RoutedEventArgs e)
        {
           AddCustomerWindow addCustomerWindow = new AddCustomerWindow();
            addCustomerWindow.Show();
        }

        private void EditCustomer_Click(object sender, RoutedEventArgs e)
        {
            //  EditWindow edit = new EditWindow();
            EditCustomerWindow edit = new EditCustomerWindow(Customers.SelectedItem as Customer);
            edit.Show();

        }

        private void RentMenu_Click(object sender, RoutedEventArgs e)
        {
            //RentWindow rentWindow = new RentWindow(this);
            //rentWindow.Show();
            NavigationService.Navigate(new System.Uri( "Pages/RentPage.xaml", UriKind.RelativeOrAbsolute));


        }

        private void InfoAboutProduct_Click(object sender, RoutedEventArgs e)
        {
            state.CurrentProduct = ProductsList.SelectedItem as Product;
            ProductWindow productWindow = new ProductWindow();
            productWindow.Show();
        }

        private void ReturnMenu_Click(object sender, RoutedEventArgs e)
        {
            //ReturnWindow returnWindow = new ReturnWindow();
            //returnWindow.Show();
            NavigationService.Navigate(new System.Uri("Pages/ReturnPage.xaml", UriKind.RelativeOrAbsolute));


        }

        private void EditProduct_Click(object sender, RoutedEventArgs e)
        {
            state.CurrentProduct = ProductsList.SelectedItem as Product;
            EditProductWindow edit = new EditProductWindow();
            edit.Show();

        }

        private void DeleteCustomer_Click(object sender, RoutedEventArgs e)
        {
           
            state.DeleteCustomer((Customers.SelectedItem as Customer).ID);
        }

        private void DeleteProduct_Click(object sender, RoutedEventArgs e)
        {
            state.DeleteProduct(ProductsList.SelectedItem as Product);
        }

        private void AddProduct_Click(object sender, RoutedEventArgs e)
        {
            AddProductWindow addProduct = new AddProductWindow();
            addProduct.Show();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new System.Uri("Pages/AuthPage.xaml", UriKind.RelativeOrAbsolute));
            
        }
    }
}
