﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lab4.Models;
using System.Collections.ObjectModel;
using Lab4.Windows;



namespace Lab4.Pages
{
    /// <summary>
    /// Логика взаимодействия для ReturnPage.xaml
    /// </summary>
    public partial class ReturnPage : Page
    {
        State state = State.getInstance();
        public ReturnPage()
        {
            InitializeComponent();
            DataContext = state;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();

        }

        private void CustomerBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ProductsBox.ItemsSource = (CustomerBox.SelectedItem as Customer).Products;
            ProductsBox.ItemsSource = (CustomerBox.SelectedItem as Customer).Products;

        }

        private void ReturnButton_Click(object sender, RoutedEventArgs e)
        {
            Customer customer = CustomerBox.SelectedItem as Customer;
            //ObservableCollection<Product> list = new ObservableCollection<Product>();
            //foreach (Product f in ProductsBox.SelectedItems)
            //{
            //    f.IsInShop = true;
            //}

            ObservableCollection<Product> products = new ObservableCollection<Product>();
            foreach (Product j in ProductsBox.SelectedItems)
                products.Add(j);

            state.ReturnProduct(customer, products);


            //for (int n = 0; n < CustomerWithProducts.Pairs.Count; n++)
            //{
            //    if (CustomerWithProducts.Pairs[n].Key.ID == customer.ID)
            //        foreach (Product f in ProductsBox.SelectedItems)
            //        {
            //            CustomerWithProducts.Pairs[n].Value.Remove(f);
            //        }
            //}
            NavigationService.GoBack();

        }
    }
}
