﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lab4.Models;
using Lab4.Windows;



namespace Lab4.Pages
{
    /// <summary>
    /// Логика взаимодействия для Page1.xaml
    /// </summary>
    public partial class AuthPage : Page
    {
        State state;
        public AuthPage()
        {
            state = State.getInstance();
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Clerk clerk = State.getInstance().Clerk;
            if (clerk.Password == PassBox.Password.ToString())
            {
                PassBox.Clear();
                WrongPassLabel.Visibility = Visibility.Hidden;
                state.Manager.isThisWorker = false;
                state.Clerk.isThisWorker = true;
                this.NavigationService.Navigate(new System.Uri("Pages/ClerkPage.xaml", UriKind.RelativeOrAbsolute));
            }
            else
            {
                WrongPassLabel.Visibility = Visibility.Visible;
            }
         //   this.NavigationService.Navigate(new System.Uri( "PassPage.xaml",UriKind.RelativeOrAbsolute));
        }

        private void ManagerButton_Click(object sender, RoutedEventArgs e)
        {

            Manager manager = State.getInstance().Manager;
            if (manager.Password == PassBox.Password.ToString())
            {
                PassBox.Clear();
                WrongPassLabel.Visibility = Visibility.Hidden;
                state.Manager.isThisWorker = true;
                state.Clerk.isThisWorker = false;
                this.NavigationService.Navigate(new System.Uri("Pages/ClerkPage.xaml", UriKind.RelativeOrAbsolute));
            }
            else
            {
                WrongPassLabel.Visibility = Visibility.Visible;
            }
        }
    }
}
