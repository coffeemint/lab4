﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Lab4.Models;
using System.Collections.ObjectModel;
using Lab4.Windows;


namespace Lab4.Pages
{
    /// <summary>
    /// Логика взаимодействия для RentPage.xaml
    /// </summary>
    /// 
    public partial class RentPage : Page
    {

        State state = State.getInstance();



        public RentPage()
        {

            DataContext = state;
            
            InitializeComponent();
            CustomerBox.SelectedIndex = 0;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.GoBack();

        }

        private void RentClick(object sender, RoutedEventArgs e)
        {
            ObservableCollection<Product> list = new ObservableCollection<Product> ();

            foreach(object T in ProductsBox.SelectedItems)
            {
                list.Add(T as Product);
            }


            if (list != null)
            {
                state.RentProduct(CustomerBox.SelectedValue.ToString(), list);
                MessageBox.Show("Owed is " + state.CountOwed(list));
                NavigationService.GoBack();

            }
        }
    }
}
