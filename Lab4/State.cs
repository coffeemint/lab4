﻿using Lab4.Models;
using Lab4.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace Lab4
{
    class State
    {
        private static State instance;

        public Product CurrentProduct { get; set; }
        public Clerk Clerk { get; set; } 
        public Manager Manager { get; set; } 
        public ObservableCollection<Product> Products { get; set; }

        public ObservableCollection<Customer> Customers { get; set; }
        public ObservableCollection<Product> AvailableProducts { get { return availableProducts(); } private set { } }


        private State()
        {
            //Clerk = ModelParser<Clerk>.LoadJsonItem(Properties.Resources.clerk);
            //Manager = ModelParser<Manager>.LoadJsonItem(Properties.Resources.manager);
            //Products = ModelParser<Product>.LoadJsonItems(Properties.Resources.Products);
            //Products = ModelParser<Product>.LoadJsonItems(Properties.Resources.Products);
            //Customers = ModelParser<Customer>.LoadJsonItems(Properties.Resources.Customer);
            Clerk = ModelParser<Clerk>.LoadJsonItem("clerk.json");
            Manager = ModelParser<Manager>.LoadJsonItem("manager.json");
            Products = ModelParser<Product>.LoadJsonItems("Products.json");
           // Products = ModelParser<Product>.LoadJsonItems("Products.json");
            Customers = ModelParser<Customer>.LoadJsonItems("customer.json");
            AvailableProducts = availableProducts();
          //  AvailableProducts = availableProducts();
           // CustomerWithProducts.Initial();
        }

        private ObservableCollection<Product> availableProducts()
        {
            ObservableCollection<Product> newProducts = new ObservableCollection<Product>();
            foreach (Product f in Products)
            {
                if (f.IsInShop == true)
                    newProducts.Add(f);
            }
            return newProducts;

        }

        //private ObservableCollection<Product> availableProducts()
        //{
        //    ObservableCollection<Product> Products = new ObservableCollection<Product>();
        //    foreach (Product g in Products)
        //    {
        //        if (g.IsInShop == true)
        //            Products.Add(g);
        //    }
        //    return Products;

        //}

        public void AddCustomer(Customer Customer)
        {
            if (Customers == null)
                Customers = new ObservableCollection<Customer>();
            //Customers.Add(Customer);
            Customers.Add(Customer);
        }

        public Customer SearchCustomerByID(string ID)
        {
            foreach (Customer Customer in Customers)
            {
                if (Customer.ID == ID)
                {
                    return Customer;
                }
            }
            return null;
        }

        public bool EditCustomer(string ID, string name, string number, string adress)
        {

            foreach (Customer Customer in Customers)
            {
                if (Customer.ID == ID)
                {
                    Customer.Name = name;
                    Customer.PhoneNumber = number;
                    Customer.Adress = adress;
                    return true;
                 }
            }

            return false;
        }


        public static State getInstance()
        {
            if (instance == null)
                instance = new State();
            return instance;
        }

        public ObservableCollection<Product> GetProducts(ObservableCollection<Product> list)
        {
            ObservableCollection<Product> newProducts = new ObservableCollection<Product>();
            foreach (Product p in list)
                foreach (Product f in Products)
                    if (p.ID == f.ID)
                        newProducts.Add(f);
            return newProducts;
        }
        
        //public void RenewProductsList()
        //{
        //    List<KeyValuePair<Customer, ObservableCollection<Product>>> pairs = CustomerWithProducts.Pairs;

        //    for (int i = 0; i< CustomerWithProducts.Pairs.Count; i++)
        //    {

        //        for (int j = 0; j < Customers.Count; j++)
        //        {

        //            if (pairs[i].Key.ID == Customers[j].ID)
        //            {
        //                Customers[j].Products = new ObservableCollection<Product>();
        //                foreach (Product product in pairs[i].Value)
        //                {
        //                    Customers[j].Products.Add(product);
        //                }
        //                break;
        //            }

        //        }

        //    }



        //    //Customer c = new Customer();
        //    //ObservableCollection<Product> ps = new ObservableCollection<Product>();

        //    //foreach (KeyValuePair<Customer,ObservableCollection<Product>> keyValuePair in CustomerWithProducts.Pairs)
        //    //{
        //    //    foreach (Customer customer in Customers)
        //    //    {
        //    //        if (keyValuePair.Key.ID == customer.ID)
        //    //        { 
        //    //            c = customer;
                    
        //    //            foreach (Product p in keyValuePair.Value)
        //    //            {
        //    //                ps.Add(p);
        //    //                //if (c.Products == null)
        //    //                //    c.Products = new ObservableCollection<Product>();
        //    //                //c.Products.Add(p);
        //    //            }
        //    //            break;

        //    //        }
        //    //    }
        //    //}

        //    //foreach (Product p in ps)
        //    //{
        //    //    if (c.Products == null)
        //    //        c.Products = new ObservableCollection<Product>();
        //    //    c.Products.Add(p);
        //    //}
        //}

        public void ReturnProduct(Customer customer, ObservableCollection<Product> products)
        {
      //      Customer c = SearchCustomerByID(customer.ID);
            ObservableCollection<Product> newProd = GetProducts(products);
            for (int i = 0; i < newProd.Count; i++)
            {
                customer.Owed -= newProd[i].DayPrice;
                newProd[i].IsInShop = true;
                customer.Remove(newProd[i]);
            }
        }


        //public static void Renew(Customer cust, ObservableCollection<Product> list)
        //{

        //    //bool isExistCust = false;
        //    //KeyValuePair<Customer, ObservableCollection<Product>> newPair = new KeyValuePair<Customer, ObservableCollection<Product>>();

        //    //foreach (KeyValuePair<Customer, ObservableCollection<Product>> p in Pairs)
        //    //{
        //    //    if (p.Key.ID == cust.ID)
        //    //    {
        //    //        isExistCust = true;
        //    //        newPair = p;
        //    //        break;

        //    //    }
        //    //}

        //    //if (isExistCust)
        //    //{
        //    //    foreach (Product prod in list)
        //    //        newPair.Value.Add(prod);
        //    //}
        //    //else
        //    //{
        //    //    KeyValuePair<Customer, ObservableCollection<Product>> keyValuePair = new KeyValuePair<Customer, ObservableCollection<Product>>(cust, list);
        //    //    Pairs.Add(keyValuePair);
        //    //}
        //}

        public void RentProduct (string ID, ObservableCollection<Product> prod)
        {
            Customer c = SearchCustomerByID(ID);
            ObservableCollection<Product> products = GetProducts(prod);
            for (int i = 0; i < products.Count; i++)
            {
                c.Owed += products[i].DayPrice;
                c.Products.Add(products[i]);
                products[i].IsInShop = false;
            }
        }

        public Customer SearchCustomerByProduct (Product product)
        {
            foreach (Customer c in Customers)
                foreach (Product p in c.Products)
                    if (p.ID == product.ID)
                        return c;
            return null;
        }

        public int CountOwed (ObservableCollection<Product> products)
        {
            int n = 0;
            foreach (Product p in products)
                n += p.DayPrice;
            return n;
        }

        public void DeleteCustomer (string ID)
        {
            for (int i = 0; i < Customers.Count; i++)
            {
                if (Customers[i].ID == ID)
                {
                    for (int j = 0; j < Customers[i].Products.Count; j++)
                        Customers[i].Products[j].IsInShop = true;
                    Customers.RemoveAt(i);
                    break;
                }

            }
        }

        public void DeleteProduct (Product product)
        {

            Customer customer = SearchCustomerByProduct(product);
            if (customer != null)
                customer.Products.Remove(product);
            Products.Remove(product);
        }

        //public Customer SearchCustomerByProduct()
        //{
        //    foreach (Customer c in Customers)
        //        foreach (Product p in c.Products)
        //            if (p.ID == CurrentProduct.ID)
        //                return c;
        //    return null;
        //}
    }
}
